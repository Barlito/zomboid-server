function SpawnRegions()
	return {
		{ name = "Riverside, KY", file = "media/maps/Riverside, KY/spawnpoints.lua" },
		{ name = "Rosewood, KY", file = "media/maps/Rosewood, KY/spawnpoints.lua" },
		{ name = "West Point, KY", file = "media/maps/West Point, KY/spawnpoints.lua" },
		{ name = "Muldraugh, KY", file = "media/maps/Muldraugh, KY/spawnpoints.lua" },
		{ name = "BedfordFalls", file = "media/maps/BedfordFalls/spawnpoints.lua" },
		{ name = "North", file = "media/maps/North/spawnpoints.lua" },
		{ name = "South", file = "media/maps/South/spawnpoints.lua" },
		{ name = "West", file = "media/maps/West/spawnpoints.lua" },
		{ name = "RavenCreek", file = "media/maps/RavenCreek/spawnpoints.lua" },
	}
end
