stack_name=zomboid-server

# Container
app_container_id = $(shell docker ps --filter name="$(stack_name)" -q)

.PHONY: bash
bash:
	docker exec -it -u root $(app_container_id) bash

.PHONY: deploy
deploy:
	mkdir -p config
	mkdir -p SteamApps
	docker-compose up -d

.PHONY: deploy-debug
deploy-debug:
	docker-compose -f docker-compose-debug.yaml up -d

.PHONY: undeploy
undeploy:
	docker-compose down

.PHONY: undeploy-debug
undeploy-debug:
	docker-compose -f docker-compose-debug.yaml down

.Phone: restart
restart:
	make undeploy
	make deploy

.PHONY: logs
logs:
	docker-compose logs -f
