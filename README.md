For the first time you launch the server : 

steamcmd

login anonymous

app_update 380870 -beta b41multiplayer validate

quit

cd .steam/SteamApps/common/Project\ Zomboid\ Dedicated\ Server/

bash start-server.sh

And type the admin password


Then stop it and update docker-compose entrypoint with : 

entrypoint: /bin/bash start-server.sh

and add 
working_dir: /root/.steam/SteamApps/common/Project\ Zomboid\ Dedicated\ Server/